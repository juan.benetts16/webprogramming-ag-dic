from django.urls import path
from investigacion import views

urlpatterns = [
	path('list/', views.List, name="inv_list"),
	path('detail/<int:pk>/', views.Detail.as_view(), name="inv_detail"),
	path('create/', views.Create.as_view(), name="inv_create"),
	path('update/<int:pk>/', views.Update.as_view(), name="inv_update"),
	path('delete/<int:pk>/', views.Delete.as_view(), name="inv_delete"),
]