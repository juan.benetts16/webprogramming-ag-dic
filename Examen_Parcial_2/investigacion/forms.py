from django import forms

from .models import Investigacion , Categoria

class InvestigacionForm(forms.ModelForm):
	class Meta:
		model = Investigacion
		fields = [
			"titulo",
			"categoria",
			"num_paginas",
			"slug",
		]

class CategoriaForm(forms.ModelForm):
	class Meta:
		model = Categoria
		fields = [
			"nombre",
		]
