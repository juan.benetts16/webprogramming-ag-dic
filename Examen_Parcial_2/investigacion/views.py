from django.shortcuts import render

from .models import Investigacion, Categoria

from django.views import generic
from .forms import InvestigacionForm
from django.urls import reverse_lazy

# Create your views here.
# RETRIEVE

def List(request):
	object_list = Investigacion.objects.all()
	categorias = Categoria.objects.all()
	if request.method == 'POST':
		cat = request.POST.get('categoria')
		print(cat , 'Valor')
		categoria = Categoria.objects.get(nombre = cat)
		object_list = Investigacion.objects.filter(categoria = categoria )
		print(categoria , 'Dato')
	else:
		print('NO entro')
	context = {  
	'categorias': categorias,
	'object_list' : object_list,
	}
	return render(request , 'investigacion/list.html' , context)


class Detail(generic.DetailView):
	template_name = "investigacion/detail.html"
	model = Investigacion

class Create(generic.CreateView):
	template_name = "investigacion/create.html"
	model = Investigacion
	fields = [
			"titulo",
			"categoria",
			"num_paginas",
			"slug",
		]
	success_url = reverse_lazy("inv_list")

class Update(generic.UpdateView):
	template_name = "investigacion/update.html"
	model = Investigacion
	fields = [
			"titulo",
			"categoria",
			"num_paginas",
			"slug",
		]
	success_url = reverse_lazy("inv_list")

class Delete(generic.DeleteView):
	template_name = "investigacion/delete.html"
	model = Investigacion
	success_url = reverse_lazy("inv_list")

