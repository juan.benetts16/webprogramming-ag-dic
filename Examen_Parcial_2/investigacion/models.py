from django.db import models

# Create your models here.
from django.conf import settings

#user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=1)

class Categoria(models.Model):
	nombre = models.CharField(max_length = 50)	
	def __str__(self):
		return self.nombre

class Investigacion(models.Model):
	investigador = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	titulo = models.CharField(max_length=24)
	categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
	num_paginas = models.IntegerField()
	fecha = models.DateField(auto_now_add=True)
	slug = models.SlugField()

	def __str__(self):
		return self.titulo



