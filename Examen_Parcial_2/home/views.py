from django.shortcuts import render

from django.contrib.auth import authenticate , login

from .forms import LoginForm

# Create your views here.
def Login(request):
	message = "Not Login"
	form = LoginForm(request.POST or None)
	if request.method == "POST":
		form = LoginForm(request.POST or None)
		if form.is_valid():
			username = request.POST["username"]
			password = request.POST["password"]
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request , user)
					message = "User Logged"
				else:
					message = "User is Not Active"
			else:
				message = "Username or Password is not Correct"
	context = {
		"form": form,
		"message": message
	}
	return render(request, "home/login.html", context)