from rest_framework import serializers

from .models import Cafes

class CafesSerialize(serializers.ModelSerializer):
	class Meta:
		model = Cafes
		fields = [
		"nombre",
		"calidad",
		"tostado",
		"origen",
		"grano",
		"slug",
		]