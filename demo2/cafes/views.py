from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Cafes
from .forms import CafesForm

#Serializers
from .serializers import CafesSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class CafesAPICreate(generics.CreateAPIView):
	serializer_class = CafesSerialize

	def perform_create(self, serializer):
		serializer.save()

class CafesAPIList(generics.ListAPIView):
	serializer_class = CafesSerialize

	def get_queryset(self, *args, **kwargs):
		return Cafes.objects.all()
#CRUD

def create(request):
	form = CafesForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "cafes/create.html", context)


#Retrieve
def list(request):
	queryset = Cafes.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'cafes/list.html', context)

def detail(request, id):
	queryset = Cafes.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'cafes/detail.html', context)

#UPDATE

def update(request, id):
	cafes = Cafes.objects.get(id=id)
	if(request.method == "GET"):
		form = CafesForm(instance = cafes)
	else:
		form = CafesForm(request.POST, instance = cafes)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'cafes/update.html', context)

#DELETE

def delete(request, id):
	cafes = Cafes.objects.get(id = id)
	if(request.method == "POST"):
		cafes.delete()
		return redirect("list")
	context = {
		"cafes": cafes,
	}
	return render(request, 'cafes/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "cafes/list2.html"
	queryset = Cafes.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Cafes.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(calidad__icontains=query) | Q(tostado__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "cafes/detail2.html"
	model = Cafes

class Create(generic.CreateView):
	template_name = "cafes/create2.html"
	model = Cafes
	fields = [
		"nombre",
		"calidad",
		"tostado",
		"origen",
		"grano",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "cafes/update2.html"
	model = Cafes
	fields = [
		"nombre",
		"calidad",
		"tostado",
		"origen",
		"grano",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "cafes/delete2.html"
	model = Cafes
	success_url = reverse_lazy("list")