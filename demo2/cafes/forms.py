from django import forms

from .models import Cafes

class CafesForm(forms.ModelForm):
	class Meta:
		model = Cafes
		fields = [
		"nombre",
		"calidad",
		"tostado",
		"origen",
		"grano",
		"slug",
		]