from django.contrib import admin

# Register your models here.

from .models import Cafes

@admin.register(Cafes)
class CafesAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"calidad",
		"tostado",
		"origen",
		"grano",
		"slug",
		]