from .models import Cafes
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Cafes ALPHA" ,}
	return context

def countCafes(request):
	context = { "countCafes" : Cafes.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context