from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from cafes import signals
from cafes import validators
# Create your models here.

class Cafes_Queryset(models.QuerySet):
	def cafes_grano(self):
		return self.filter(grano = True)

class Cafes(models.Model):
	nombre = models.CharField(max_length = 20)
	calidad = models.IntegerField(validators = [validators.validation_calidad])
	tostado = models.CharField(max_length = 20)
	origen = models.CharField(max_length = 100)
	grano = models.BooleanField()
	slug = models.CharField(max_length = 15)
	objects = Cafes_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_cafes_receiver , sender = Cafes)
pre_save.connect(signals.pre_save_cafes_receiver , sender = Cafes)
pre_delete.connect(signals.post_delete_cafes_receiver , sender = Cafes)
post_delete.connect(signals.pre_delete_cafes_receiver  , sender = Cafes)