from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from mascotas import signals
from mascotas import validators
# Create your models here.

class Mascotas_Queryset(models.QuerySet):
	def mascotas_edad(self):
		return self.filter(edad = 4)
	def mascotas_estado(self):
		return self.filter(estado = False)

class Mascotas(models.Model):
	nombre = models.CharField(max_length = 20)
	tipo = models.CharField(max_length = 20)
	edad = models.IntegerField()
	raza = models.CharField(max_length = 100)
	estado = models.BooleanField(validators = [validators.validation_estado])
	slug = models.CharField(max_length = 15)
	objects = Mascotas_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_mascotas_receiver , sender = Mascotas)
pre_save.connect(signals.pre_save_mascotas_receiver , sender = Mascotas)
pre_delete.connect(signals.post_delete_mascotas_receiver , sender = Mascotas)
post_delete.connect(signals.pre_delete_mascotas_receiver  , sender = Mascotas)