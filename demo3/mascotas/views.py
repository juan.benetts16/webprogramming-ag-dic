from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Mascotas
from .forms import MascotasForm

#Serializers
from .serializers import MascotasSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class MascotasAPICreate(generics.CreateAPIView):
	serializer_class = MascotasSerialize

	def perform_create(self, serializer):
		serializer.save()

class MascotasAPIList(generics.ListAPIView):
	serializer_class = MascotasSerialize

	def get_queryset(self, *args, **kwargs):
		return Mascotas.objects.all()

#CRUD

#CREATE
def create(request):
	form = MascotasForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "mascotas/create.html", context)


#Retrieve
def list(request):
	queryset = Mascotas.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'mascotas/list.html', context)

def detail(request, id):
	queryset = Mascotas.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'mascotas/detail.html', context)

#UPDATE

def update(request, id):
	mascotas = Mascotas.objects.get(id=id)
	if(request.method == "GET"):
		form = MascotasForm(instance = mascotas)
	else:
		form = MascotasForm(request.POST, instance = mascotas)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'mascotas/update.html', context)

#DELETE

def delete(request, id):
	mascotas = Mascotas.objects.get(id = id)
	if(request.method == "POST"):
		mascotas.delete()
		return redirect("list")
	context = {
		"mascotas": mascotas,
	}
	return render(request, 'mascotas/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "mascotas/list2.html"
	queryset = Mascotas.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Mascotas.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(tipo__icontains=query) | Q(raza__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "mascotas/detail2.html"
	model = Mascotas

class Create(generic.CreateView):
	template_name = "mascotas/create2.html"
	model = Mascotas
	fields = [
		"nombre",
		"tipo",
		"edad",
		"raza",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "mascotas/update2.html"
	model = Mascotas
	fields = [
		"nombre",
		"tipo",
		"edad",
		"raza",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "mascotas/delete2.html"
	model = Mascotas
	success_url = reverse_lazy("list")