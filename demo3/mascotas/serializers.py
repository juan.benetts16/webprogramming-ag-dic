from rest_framework import serializers

from .models import Mascotas

class MascotasSerialize(serializers.ModelSerializer):
	class Meta:
		model = Mascotas
		fields = [
		"nombre",
		"tipo",
		"edad",
		"raza",
		"estado",
		"slug",
		]