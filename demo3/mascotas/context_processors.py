from .models import Mascotas
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Mascotas ALPHA" ,}
	return context

def countMascotas(request):
	context = { "countMascotas" : Mascotas.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context