from django.contrib import admin

# Register your models here.

from .models import Mascotas

@admin.register(Mascotas)
class MascotasAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"tipo",
		"edad",
		"raza",
		"estado",
		"slug",
		]