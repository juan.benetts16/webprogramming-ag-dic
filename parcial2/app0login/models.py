from django.db import models

from django.conf import settings

# Create your models here.

class Login(models.Model):
	username = models.CharField(max_length = 20)
	password = models.CharField(max_length = 50)
	slug = models.CharField(max_length = 50)

	def __str__(self):
		return self.username