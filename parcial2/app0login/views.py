from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.views import generic
from django.urls import reverse_lazy
from .forms import LoginForm

# Create your views here.

#LOGIN
def index(request):
	message = "No Loggeado"
	form = LoginForm(request.POST or None)
	if request.method == "POST":
		form = LoginForm(request.POST or None)
		if form.is_valid():
			username = request.POST["username"]
			password = request.POST["password"]
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request, user)
					message = "Usuario Logeado"
				else:
					message = "Usuario no Loggeado"
			else:
				message = "Usuario o Contrasena incorrecto"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "app0/index.html", context)

class Signup(generic.FormView):
	template_name = 'app0/signup.html'
	success_url = reverse_lazy('login')

	def form_valid(self, form):
		user = form.save()
		return super(Signup, self).form_valid(form)


