from django import forms

from .models import Investigaciones, Categorias

class InvestigacionesForm(forms.ModelForm):
	class Meta:
		model = Investigaciones
		fields = [
		"titulo",
		"autor",
		"num_paginas",
		"categoria",
		"fecha",
		"slug",
		]

class CategoriasForm(forms.ModelForm):
	class Meta:
		model = Categorias
		fields = [
		"nombre",
		"slug",
		]