from django.db import models

from django.conf import settings

# Create your models here.

class Categorias(models.Model):
	nombre = models.CharField(max_length = 100)
	slug = models.CharField(max_length = 50)

	def __str__(self):
		return self.nombre

class Investigaciones(models.Model):
	titulo = models.CharField(max_length = 100)
	autor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE)
	num_paginas = models.IntegerField()
	categoria = models.ForeignKey(Categorias, on_delete = models.CASCADE)
	fecha = models.DateField()
	slug = models.CharField(max_length = 50)


	def __str__(self):
		return self.titulo

