from django.urls import path, include
from app0 import views

urlpatterns = [
	#Generics
	path('list2/', views.List.as_view(), name = "list2"),
	path('detail2/<int:pk>/', views.Detail.as_view(), name = "detail2"),
	path('create2/', views.Create.as_view(), name = "create2"),
	path('update2/<int:pk>/', views.Update.as_view(), name = "update2"),
]