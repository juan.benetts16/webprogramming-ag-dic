from .models import Celulares
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Celulares ALPHA" ,}
	return context

def countCelulares(request):
	context = { "countCelulares" : Celulares.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context