from rest_framework import serializers

from .models import Celulares

class CelularesSerialize(serializers.ModelSerializer):
	class Meta:
		model = Celulares
		fields = [
		"nombre",
		"marca",
		"ram",
		"memoria",
		"estado",
		"slug",
		]