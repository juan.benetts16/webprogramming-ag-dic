from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Celulares
from .forms import CelularesForm

#Serializers
from .serializers import CelularesSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class CelularesAPICreate(generics.CreateAPIView):
	serializer_class = CelularesSerialize

	def perform_create(self, serializer):
		serializer.save()

class CelularesAPIList(generics.ListAPIView):
	serializer_class = CelularesSerialize

	def get_queryset(self, *args, **kwargs):
		return Celulares.objects.all()

#CRUD

#CREATE
def create(request):
	form = CelularesForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "celulares/create.html", context)


#Retrieve
def list(request):
	queryset = Celulares.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'celulares/list.html', context)

def detail(request, id):
	queryset = Celulares.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'celulares/detail.html', context)

#UPDATE

def update(request, id):
	celulares = Celulares.objects.get(id=id)
	if(request.method == "GET"):
		form = CelularesForm(instance = celulares)
	else:
		form = CelularesForm(request.POST, instance = celulares)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'celulares/update.html', context)

#DELETE

def delete(request, id):
	celulares = Celulares.objects.get(id = id)
	if(request.method == "POST"):
		celulares.delete()
		return redirect("list")
	context = {
		"celulares": celulares,
	}
	return render(request, 'celulares/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "celulares/list2.html"
	queryset = Celulares.objects.filter()
	def get_queryset(self, *args, **kwargs):
		qs = Celulares.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(marca__icontains=query) | Q(ram__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "celulares/detail2.html"
	model = Celulares

class Create(generic.CreateView):
	template_name = "celulares/create2.html"
	model = Celulares
	fields = [
		"nombre",
		"marca",
		"ram",
		"memoria",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "celulares/update2.html"
	model = Celulares
	fields = [
		"nombre",
		"marca",
		"ram",
		"memoria",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "celulares/delete2.html"
	model = Celulares
	success_url = reverse_lazy("list")
