from django.contrib import admin

# Register your models here.

from .models import Celulares

@admin.register(Celulares)
class CelularesAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"marca",
		"ram",
		"memoria",
		"estado",
		"slug",
		]
