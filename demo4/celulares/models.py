from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from celulares import signals
from celulares import validators
# Create your models here.

class Celulares_Queryset(models.QuerySet):
	def celulares_marca(self):
		return self.filter(marca = "Apple")
	def celulares_ram(self):
		return self.filter(ram = 8)

class Celulares(models.Model):
	nombre = models.CharField(max_length = 20)
	marca = models.CharField(max_length = 20)
	ram = models.IntegerField(validators = [validators.validation_ram])
	memoria = models.IntegerField()
	estado = models.BooleanField(validators = [validators.validation_estado])
	slug = models.CharField(max_length = 15)
	objects = Celulares_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_celulares_receiver , sender = Celulares)
pre_save.connect(signals.pre_save_celulares_receiver , sender = Celulares)
pre_delete.connect(signals.post_delete_celulares_receiver , sender = Celulares)
post_delete.connect(signals.pre_delete_celulares_receiver  , sender = Celulares)