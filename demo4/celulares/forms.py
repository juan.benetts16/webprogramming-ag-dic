from django import forms

from .models import Celulares

class CelularesForm(forms.ModelForm):
	class Meta:
		model = Celulares
		fields = [
		"nombre",
		"marca",
		"ram",
		"memoria",
		"estado",
		"slug",
		]