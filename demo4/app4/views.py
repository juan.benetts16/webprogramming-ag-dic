from django.shortcuts import render

from .forms import LoginForm, Users_Form
from django.contrib.auth import authenticate, login 

from django.views import generic
from django.urls import reverse_lazy

# Create your views here.
class SignUp(generic.FormView):
	template_name = "app4/signup.html"
	form_class = Users_Form
	success_url = reverse_lazy('login')


	def form_valid(self , form):
		user = form.save()
		return super(SignUp, self).form_valid(form) 

def index(request):
	message = "Not login"
	form = LoginForm(request.POST or None )
	if(request.method == "POST"):
		if(form.is_valid()):
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username = username , password = password)
			if user is not None:
				if user.is_active:
					login(request , user )
					message = 'Usuario Loggeado'
				else:
					message = 'Usuario no Activo'
			else:
				message = 'No existe el usuario'

	context = {
		'form': form,
		'message': message,
	}
	return render(request, "app4/index.html", context)

def index1(request):
	return render(request,'app4/index1.html', {})

def index2(request):
	return render(request, 'app4/index2.html', {})

def index3(request):
	return render(request, 'app4/index3.html', {})

def index4(request):
	return render(request, 'app4/index4.html', {})

def index5(request):
	return render(request, 'app4/index5.html', {})

def index6(request):
	return render(request, 'app4/index6.html', {})

def index7(request):
	return render(request, 'app4/index7.html', {})

def index8(request):
	return render(request, 'app4/index8.html', {})

def index9(request):
	return render(request, 'app4/index9.html', {})

def index10(request):
	return render(request, 'app4/index10.html', {})