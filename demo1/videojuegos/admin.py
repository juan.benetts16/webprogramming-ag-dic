from django.contrib import admin

# Register your models here.

from .models import Videojuegos

@admin.register(Videojuegos)
class VideojuegosAdmin(admin.ModelAdmin):
	list_display = [
		'nombre',
		'rate',
		'salida',
		'genero',
		'multiplayer',
		'slug',
	]