from .models import Videojuegos
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Videojuegos ALPHA" ,}
	return context

def countVideo(request):
	context = { "countVideo" : Videojuegos.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context