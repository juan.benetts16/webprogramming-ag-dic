from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from videojuegos import signals
from videojuegos import validators
# Create your models here.

class Videojuegos_Queryset(models.QuerySet):
	def videojuegos_edad(self):
		return self.filter(nombre = "osu!")
	def videojuegos_rate(self):
		return self.filter(rate = 10)
	def videojuegos_multiplayer(self):
		return self.filter(multiplayer = True)

class Videojuegos(models.Model):
	nombre = models.CharField(max_length = 20)
	rate = models.IntegerField(validators = [validators.validation_rate])
	salida = models.CharField(max_length = 20)
	genero = models.CharField(max_length = 100)
	multiplayer = models.BooleanField()
	slug = models.CharField(max_length = 15)
	objects = Videojuegos_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_videojuegos_receiver , sender = Videojuegos)
pre_save.connect(signals.pre_save_videojuegos_receiver , sender = Videojuegos)
pre_delete.connect(signals.post_delete_videojuegos_receiver , sender = Videojuegos)
post_delete.connect(signals.pre_delete_videojuegos_receiver  , sender = Videojuegos)
