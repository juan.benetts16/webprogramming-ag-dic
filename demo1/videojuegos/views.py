from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Videojuegos
from .forms import VideojuegosForm

#Serializers
from .serializers import VideojuegosSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class VideojuegosAPICreate(generics.CreateAPIView):
	serializer_class = VideojuegosSerialize

	def perform_create(self, serializer):
		serializer.save()

class VideojuegosAPIList(generics.ListAPIView):
	serializer_class = VideojuegosSerialize

	def get_queryset(self, *args, **kwargs):
		return Videojuegos.objects.all()
#CRUD
#CREATE
def create(request):
	form = VideojuegosForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "videojuegos/create.html", context)


#Retrieve
def list(request):
	queryset = Videojuegos.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'videojuegos/list.html', context)

def detail(request, id):
	queryset = Videojuegos.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'videojuegos/detail.html', context)

#UPDATE

def update(request, id):
	videojuegos = Videojuegos.objects.get(id=id)
	if(request.method == "GET"):
		form = VideojuegosForm(instance = videojuegos)
	else:
		form = VideojuegosForm(request.POST, instance = videojuegos)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'videojuegos/update.html', context)

#DELETE

def delete(request, id):
	videojuegos = Videojuegos.objects.get(id = id)
	if(request.method == "POST"):
		videojuegos.delete()
		return redirect("list")
	context = {
		"videojuegos": videojuegos,
	}
	return render(request, 'videojuegos/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "videojuegos/list2.html"
	queryset = Videojuegos.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Videojuegos.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(rate__icontains=query) | Q(salida__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "videojuegos/detail2.html"
	model = Videojuegos

class Create(generic.CreateView):
	template_name = "videojuegos/create2.html"
	model = Videojuegos
	fields = [
		"nombre",
		"rate",
		"salida",
		"genero",
		"multiplayer",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "videojuegos/update2.html"
	model = Videojuegos
	fields = [
		"nombre",
		"rate",
		"salida",
		"genero",
		"multiplayer",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "videojuegos/delete2.html"
	model = Videojuegos
	success_url = reverse_lazy("list")

