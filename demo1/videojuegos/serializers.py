from rest_framework import serializers

from .models import Videojuegos

class VideojuegosSerialize(serializers.ModelSerializer):
	class Meta:
		model = Videojuegos
		fields = [
		"nombre",
		"rate",
		"salida",
		"genero",
		"multiplayer",
		"slug",
		]