from django.core.exceptions import ValidationError

def validation_rate(value):
	rate = value
	if (rate > 10):
		raise ValidationError("No puedes dar calificaciones +10")
	return value

