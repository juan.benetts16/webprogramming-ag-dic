from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Pokemons
from .forms import PokemonsForm

#Serializers
from .serializers import PokemonsSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class PokemonsAPICreate(generics.CreateAPIView):
	serializer_class = PokemonsSerialize

	def perform_create(self, serializer):
		serializer.save()

class PokemonsAPIList(generics.ListAPIView):
	serializer_class = PokemonsSerialize

	def get_queryset(self, *args, **kwargs):
		return Pokemons.objects.all()

#CRUD

#CREATE
def create(request):
	form = PokemonsForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "pokemons/create.html", context)


#Retrieve
def list(request):
	queryset = Pokemons.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'pokemons/list.html', context)

def detail(request, id):
	queryset = Pokemons.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'pokemons/detail.html', context)

#UPDATE

def update(request, id):
	pokemons = Pokemons.objects.get(id=id)
	if(request.method == "GET"):
		form = PokemonsForm(instance = pokemons)
	else:
		form = PokemonsForm(request.POST, instance = pokemons)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'pokemons/update.html', context)

#DELETE

def delete(request, id):
	pokemons = Pokemons.objects.get(id = id)
	if(request.method == "POST"):
		pokemons.delete()
		return redirect("list")
	context = {
		"pokemons": pokemons,
	}
	return render(request, 'pokemons/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "pokemons/list2.html"
	queryset = Pokemons.objects.filter()
	def get_queryset(self, *args, **kwargs):
		qs = Pokemons.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(tipo__icontains=query) | Q(exp__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "pokemons/detail2.html"
	model = Pokemons

class Create(generic.CreateView):
	template_name = "pokemons/create2.html"
	model = Pokemons
	fields = [
		"nombre",
		"tipo",
		"exp",
		"level",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "pokemons/update2.html"
	model = Pokemons
	fields = [
		"nombre",
		"tipo",
		"exp",
		"level",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "pokemons/delete2.html"
	model = Pokemons
	success_url = reverse_lazy("list")
