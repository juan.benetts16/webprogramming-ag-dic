from django.core.exceptions import ValidationError

def validation_level(value):
	level = value
	if (value > 60):
		raise ValidationError("No puedes ser tan grande")
	return value

