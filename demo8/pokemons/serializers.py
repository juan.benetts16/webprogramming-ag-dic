from rest_framework import serializers

from .models import Pokemons

class PokemonsSerialize(serializers.ModelSerializer):
	class Meta:
		model = Pokemons
		fields = [
		"nombre",
		"tipo",
		"exp",
		"level",
		"estado",
		"slug",
		]