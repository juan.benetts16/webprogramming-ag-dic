from .models import Pokemons
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Pokemons ALPHA" ,}
	return context

def countPokemons(request):
	context = { "countPokemons" : Pokemons.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context