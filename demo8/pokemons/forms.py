from django import forms

from .models import Pokemons

class PokemonsForm(forms.ModelForm):
	class Meta:
		model = Pokemons
		fields = [
		"nombre",
		"tipo",
		"exp",
		"level",
		"estado",
		"slug",
		]