from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from pokemons import signals
from pokemons import validators
# Create your models here.

class Pokemons_Queryset(models.QuerySet):
	def pokemons_tipo(self):
		return self.filter(tipo = "Water")
	def pokemons_level(self):
		return self.filter(level = 60)
	def pokemons_estado(self):
		return self.filter(estado = True)

class Pokemons(models.Model):
	nombre = models.CharField(max_length = 20)
	tipo = models.CharField(max_length = 20)
	exp = models.IntegerField()
	level = models.IntegerField(validators = [validators.validation_level])
	estado = models.BooleanField()
	slug = models.CharField(max_length = 15)
	objects = Pokemons_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_pokemons_receiver , sender = Pokemons)
pre_save.connect(signals.pre_save_pokemons_receiver , sender = Pokemons)
pre_delete.connect(signals.post_delete_pokemons_receiver , sender = Pokemons)
post_delete.connect(signals.pre_delete_pokemons_receiver  , sender = Pokemons)

