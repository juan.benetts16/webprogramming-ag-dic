from django.contrib import admin

# Register your models here.

from .models import Pokemons

@admin.register(Pokemons)
class PokemonsAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"tipo",
		"exp",
		"level",
		"estado",
		"slug",
		]
