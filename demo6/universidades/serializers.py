from rest_framework import serializers

from .models import Universidades

class UniversidadesSerialize(serializers.ModelSerializer):
	class Meta:
		model = Universidades
		fields = [
		"nombre",
		"nivel",
		"alumnos",
		"carreras",
		"ingenieria",
		"slug",
		]