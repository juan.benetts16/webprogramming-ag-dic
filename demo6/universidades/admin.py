from django.contrib import admin

# Register your models here.

from .models import Universidades

@admin.register(Universidades)
class UniversidadesAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"nivel",
		"alumnos",
		"carreras",
		"ingenieria",
		"slug",
		]
