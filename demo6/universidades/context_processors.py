from .models import Universidades
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Universidades ALPHA" ,}
	return context

def countUniversidades(request):
	context = { "countUniversidades" : Universidades.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context