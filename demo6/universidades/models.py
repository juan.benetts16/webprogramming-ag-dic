from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from universidades import signals
from universidades import validators
# Create your models here.

class Universidades_Queryset(models.QuerySet):
	def universidades_nivel(self):
		return self.filter(nivel = "Maestria")
	def universidades_nombre(self):
		return self.filter(nombre = "Harvard")

class Universidades(models.Model):
	nombre = models.CharField(max_length = 20)
	nivel = models.CharField(max_length = 20)
	alumnos = models.IntegerField(validators = [validators.validation_alumnos])
	carreras = models.IntegerField()
	ingenieria = models.BooleanField(validators = [validators.validation_ingenieria])
	slug = models.CharField(max_length = 15)
	objects = Universidades_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_universidades_receiver , sender = Universidades)
pre_save.connect(signals.pre_save_universidades_receiver , sender = Universidades)
pre_delete.connect(signals.post_delete_universidades_receiver , sender = Universidades)
post_delete.connect(signals.pre_delete_universidades_receiver  , sender = Universidades)

