from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Universidades
from .forms import UniversidadesForm

#Serializers
from .serializers import UniversidadesSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class UniversidadesAPICreate(generics.CreateAPIView):
	serializer_class = UniversidadesSerialize

	def perform_create(self, serializer):
		serializer.save()

class UniversidadesAPIList(generics.ListAPIView):
	serializer_class = UniversidadesSerialize

	def get_queryset(self, *args, **kwargs):
		return Universidades.objects.all()

#CRUD

#CREATE
def create(request):
	form = UniversidadesForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "universidades/create.html", context)


#Retrieve
def list(request):
	queryset = Universidades.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'universidades/list.html', context)

def detail(request, id):
	queryset = Universidades.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'universidades/detail.html', context)

#UPDATE

def update(request, id):
	universidades = Universidades.objects.get(id=id)
	if(request.method == "GET"):
		form = UniversidadesForm(instance = universidades)
	else:
		form = UniversidadesForm(request.POST, instance = universidades)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'universidades/update.html', context)

#DELETE

def delete(request, id):
	universidades = Universidades.objects.get(id = id)
	if(request.method == "POST"):
		universidades.delete()
		return redirect("list")
	context = {
		"universidades": universidades,
	}
	return render(request, 'universidades/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "universidades/list2.html"
	queryset = Universidades.objects.filter()
	def get_queryset(self, *args, **kwargs):
		qs = Universidades.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(nivel__icontains=query) | Q(alumnos__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "universidades/detail2.html"
	model = Universidades

class Create(generic.CreateView):
	template_name = "universidades/create2.html"
	model = Universidades
	fields = [
		"nombre",
		"nivel",
		"alumnos",
		"carreras",
		"ingenieria",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "universidades/update2.html"
	model = Universidades
	fields = [
		"nombre",
		"nivel",
		"alumnos",
		"carreras",
		"ingenieria",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "universidades/delete2.html"
	model = Universidades
	success_url = reverse_lazy("list")