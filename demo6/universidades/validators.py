from django.core.exceptions import ValidationError

def validation_alumnos(value):
	alumnos = value
	if (value > 10000):
		raise ValidationError("No puede haber tantos")
	return value

def validation_ingenieria(value):
	ingenieria = value
	if (value  == False):
		raise ValidationError("No aplicable")
	return value

