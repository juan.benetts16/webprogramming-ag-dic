from django import forms

from .models import Universidades

class UniversidadesForm(forms.ModelForm):
	class Meta:
		model = Universidades
		fields = [
		"nombre",
		"nivel",
		"alumnos",
		"carreras",
		"ingenieria",
		"slug",
		]