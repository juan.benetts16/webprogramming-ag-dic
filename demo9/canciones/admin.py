from django.contrib import admin

# Register your models here.

from .models import Canciones

@admin.register(Canciones)
class CancionesAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"artista",
		"cantidad",
		"salida",
		"venta",
		"slug",
		]

