from rest_framework import serializers

from .models import Canciones

class CancionesSerialize(serializers.ModelSerializer):
	class Meta:
		model = Canciones
		fields = [
		"nombre",
		"artista",
		"cantidad",
		"salida",
		"venta",
		"slug",
		]