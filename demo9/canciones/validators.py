from django.core.exceptions import ValidationError

def validation_venta(value):
	venta = value
	if (value != True):
		raise ValidationError("Tiene que estar a la venta")
	return value

def validation_cantidad(value):
	cantidad = value
	if (value < 1000):
		raise ValidationError("Tiene que tener mas")
	return value

