from django import forms

from .models import Canciones

class CancionesForm(forms.ModelForm):
	class Meta:
		model = Canciones
		fields = [
		"nombre",
		"artista",
		"cantidad",
		"salida",
		"venta",
		"slug",
		]