from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Canciones
from .forms import CancionesForm

#Serializers
from .serializers import CancionesSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class CancionesAPICreate(generics.CreateAPIView):
	serializer_class = CancionesSerialize

	def perform_create(self, serializer):
		serializer.save()


class CancionesAPIList(generics.ListAPIView):
	serializer_class = CancionesSerialize

	def get_queryset(self, *args, **kwargs):
		return Canciones.objects.all()

#CRUD

#CREATE
def create(request):
	form = CancionesForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "canciones/create.html", context)


#Retrieve
def list(request):
	queryset = Canciones.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'canciones/list.html', context)

def detail(request, id):
	queryset = Canciones.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'canciones/detail.html', context)

#UPDATE

def update(request, id):
	canciones = Canciones.objects.get(id=id)
	if(request.method == "GET"):
		form = CancionesForm(instance = canciones)
	else:
		form = CancionesForm(request.POST, instance = canciones)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'canciones/update.html', context)

#DELETE

def delete(request, id):
	canciones = Canciones.objects.get(id = id)
	if(request.method == "POST"):
		canciones.delete()
		return redirect("list")
	context = {
		"canciones": canciones,
	}
	return render(request, 'canciones/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "canciones/list2.html"
	queryset = Canciones.objects.filter()
	def get_queryset(self, *args, **kwargs):
		qs = Canciones.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(artista__icontains=query) | Q(cantidad__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "canciones/detail2.html"
	model = Canciones

class Create(generic.CreateView):
	template_name = "canciones/create2.html"
	model = Canciones
	fields = [
		"nombre",
		"artista",
		"cantidad",
		"salida",
		"venta",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "canciones/update2.html"
	model = Canciones
	fields = [
		"nombre",
		"artista",
		"cantidad",
		"salida",
		"venta",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "canciones/delete2.html"
	model = Canciones
	success_url = reverse_lazy("list")
