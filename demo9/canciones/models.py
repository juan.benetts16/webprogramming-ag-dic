from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from canciones import signals
from canciones import validators
# Create your models here.

class Canciones_Queryset(models.QuerySet):
	def canciones_venta(self):
		return self.filter(venta = True)
	def canciones_artista(self):
		return self.filter(artista = "Tyler, The Creator")

class Canciones(models.Model):
	nombre = models.CharField(max_length = 20)
	artista = models.CharField(max_length = 20)
	cantidad = models.IntegerField(validators = [validators.validation_cantidad])
	salida = models.IntegerField()
	venta = models.BooleanField(validators = [validators.validation_venta])
	slug = models.CharField(max_length = 15)
	objects = Canciones_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_canciones_receiver , sender = Canciones)
pre_save.connect(signals.pre_save_canciones_receiver , sender = Canciones)
pre_delete.connect(signals.post_delete_canciones_receiver , sender = Canciones)
post_delete.connect(signals.pre_delete_canciones_receiver  , sender = Canciones)
