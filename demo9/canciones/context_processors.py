from .models import Canciones
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Canciones ALPHA" ,}
	return context

def countCanciones(request):
	context = { "countCanciones" : Canciones.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context