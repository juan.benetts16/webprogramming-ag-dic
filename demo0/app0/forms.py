from django import forms 

from django.contrib.auth.forms import UserCreationForm

class Users_Form(UserCreationForm):
	email = forms.CharField()

class LoginForm(forms.Form):
	username = forms.CharField(max_length = 30 , widget=forms.TextInput())
	password = forms.CharField(max_length = 30 , widget = forms.PasswordInput())

	