from rest_framework import serializers

from .models import Alumnos

class AlumnosSerialize(serializers.ModelSerializer):
	class Meta:
		model = Alumnos
		fields = [
		"nombre",
		"apellido_p",
		"apellido_m",
		"direccion",
		"edad",
		"carrera",
		"slug",
		]