from django import forms

from .models import Alumnos

class AlumnosForm(forms.ModelForm):
	class Meta:
		model = Alumnos
		fields = [
		"nombre",
		"apellido_p",
		"apellido_m",
		"direccion",
		"edad",
		"carrera",
		"slug",
		]