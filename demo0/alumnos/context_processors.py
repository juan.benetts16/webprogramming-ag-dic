from .models import Alumnos
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Alumnos ALPHA" ,}
	return context

def countAlumnos(request):
	context = { "countAlum" : Alumnos.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context