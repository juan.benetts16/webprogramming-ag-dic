from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from alumnos import signals
from alumnos import validators
# Create your models here.

class Alumnos_Queryset(models.QuerySet):
	def alumno_edad(self):
		return self.filter(edad = 21)
	def alumno_carrera(self):
		return self.filter(carrera = "ISC")

class Alumnos(models.Model):
	nombre = models.CharField(max_length = 20)
	apellido_p = models.CharField(max_length = 20)
	apellido_m = models.CharField(max_length = 20)
	direccion = models.CharField(max_length = 100)
	edad = models.IntegerField(validators = [validators.validation_edad])
	carrera = models.CharField(max_length = 50)
	slug = models.CharField(max_length = 15)
	objects = Alumnos_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_alumnos_receiver , sender = Alumnos)
pre_save.connect(signals.pre_save_alumnos_receiver , sender = Alumnos)
pre_delete.connect(signals.post_delete_alumnos_receiver , sender = Alumnos)
post_delete.connect(signals.pre_delete_alumnos_receiver  , sender = Alumnos)