from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Alumnos
from .forms import AlumnosForm

#Serializers
from .serializers import AlumnosSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class AlumnosAPICreate(generics.CreateAPIView):
	serializer_class = AlumnosSerialize

	def perform_create(self, serializer):
		serializer.save()


class AlumnosAPIList(generics.ListAPIView):
	serializer_class = AlumnosSerialize

	def get_queryset(self, *args, **kwargs):
		return Alumnos.objects.all()

#CRUD
#Retrieve
def list(request):
	queryset = Alumnos.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'alumnos/list.html', context)

def detail(request, id):
	queryset = Alumnos.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'alumnos/detail.html', context)

#CREATE
def create(request):
	form = AlumnosForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "alumnos/create.html", context)

#UPDATE

def update(request, id):
	alumnos = Alumnos.objects.get(id=id)
	if(request.method == "GET"):
		form = AlumnosForm(instance = alumnos)
	else:
		form = AlumnosForm(request.POST, instance = alumnos)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'alumnos/update.html', context)

#DELETE

def delete(request, id):
	alumnos = Alumnos.objects.get(id = id)
	if(request.method == "POST"):
		alumnos.delete()
		return redirect("list")
	context = {
		"alumnos": alumnos,
	}
	return render(request, 'alumnos/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "alumnos/list2.html"
	queryset = Alumnos.objects.filter()

	def get_queryset(self, *args, **kwargs):
		qs = Alumnos.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(apellido_p__icontains=query) | Q(apellido_m__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "alumnos/detail2.html"
	model = Alumnos

class Create(generic.CreateView):
	template_name = "alumnos/create2.html"
	model = Alumnos
	fields = [
		"nombre",
		"apellido_p",
		"apellido_m",
		"direccion",
		"edad",
		"carrera",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "alumnos/update2.html"
	model = Alumnos
	fields = [
		"nombre",
		"apellido_p",
		"apellido_m",
		"direccion",
		"edad",
		"carrera",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "alumnos/delete2.html"
	model = Alumnos
	success_url = reverse_lazy("list")





