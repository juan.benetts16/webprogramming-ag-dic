from django.contrib import admin

# Register your models here.

from .models import Alumnos

@admin.register(Alumnos)
class AlumnosAdmin(admin.ModelAdmin):
	list_display = [
		'nombre',
		'apellido_p',
		'apellido_m',
		'direccion',
		'edad',
		'carrera',
		'slug',
	]

