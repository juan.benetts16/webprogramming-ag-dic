from django.urls import path, include
from libros import views

urlpatterns = [
	path('list/', views.list, name = 'list'),
	path('detail/<int:id>/', views.detail, name = 'detail'),
	path('create/', views.create, name = "create"),
	path('update/<int:id>/', views.update, name = "update"),
	path('delete/<int:id>/', views.delete, name = "delete"),
	#Generics
	path('list2/', views.List.as_view(), name = "list2"),
	path('detail2/<int:pk>/', views.Detail.as_view(), name = "detail2"),
	path('create2/', views.Create.as_view(), name = "create2"),
	path('update2/<int:pk>/', views.Update.as_view(), name = "update2"),
	path('delete2/<int:pk>/', views.Delete.as_view(), name = "delete2"),
	#Serializers
	path('api_list', views.LibrosAPIList.as_view(), name = "serialize_list"),
	path('api_create', views.LibrosAPICreate.as_view(), name = "serialize_create"),
]