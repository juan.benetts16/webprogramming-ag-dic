from rest_framework import serializers

from .models import Libros

class LibrosSerialize(serializers.ModelSerializer):
	class Meta:
		model = Libros
		fields = [
		"nombre",
		"autor",
		"paginas",
		"cantidad",
		"electronico",
		"slug",
		]