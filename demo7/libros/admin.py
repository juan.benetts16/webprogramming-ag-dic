from django.contrib import admin

# Register your models here.

from .models import Libros

@admin.register(Libros)
class LibrosAdmin(admin.ModelAdmin):
	list_display = [
		"nombre",
		"autor",
		"paginas",
		"cantidad",
		"electronico",
		"slug",
		]
