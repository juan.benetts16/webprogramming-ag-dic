from django import forms

from .models import Libros

class LibrosForm(forms.ModelForm):
	class Meta:
		model = Libros
		fields = [
		"nombre",
		"autor",
		"paginas",
		"cantidad",
		"electronico",
		"slug",
		]