from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from libros import signals
from libros import validators
# Create your models here.

class Libros_Queryset(models.QuerySet):
	def libros_electronico(self):
		return self.filter(electronico = True)

class Libros(models.Model):
	nombre = models.CharField(max_length = 20)
	autor = models.CharField(max_length = 20)
	paginas = models.IntegerField(validators = [validators.validation_paginas])
	cantidad = models.IntegerField()
	electronico = models.BooleanField(validators = [validators.validation_electronico])
	slug = models.CharField(max_length = 15)
	objects = Libros_Queryset.as_manager()

	def __str__(self):
		return self.nombre

#Signals
post_save.connect(signals.post_save_libros_receiver , sender = Libros)
pre_save.connect(signals.pre_save_libros_receiver , sender = Libros)
pre_delete.connect(signals.post_delete_libros_receiver , sender = Libros)
post_delete.connect(signals.pre_delete_libros_receiver  , sender = Libros)
