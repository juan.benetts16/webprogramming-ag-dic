from .models import Libros
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Libros ALPHA" ,}
	return context

def countLibros(request):
	context = { "countLibros" : Libros.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context