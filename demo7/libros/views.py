from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Libros
from .forms import LibrosForm

#Serializers
from .serializers import LibrosSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class LibrosAPICreate(generics.CreateAPIView):
	serializer_class = LibrosSerialize

	def perform_create(self, serializer):
		serializer.save()

class LibrosAPIList(generics.ListAPIView):
	serializer_class = LibrosSerialize

	def get_queryset(self, *args, **kwargs):
		return Libros.objects.all()

#CRUD

#CREATE
def create(request):
	form = LibrosForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "libros/create.html", context)


#Retrieve
def list(request):
	queryset = Libros.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'libros/list.html', context)

def detail(request, id):
	queryset = Libros.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'libros/detail.html', context)

#UPDATE

def update(request, id):
	libros = Libros.objects.get(id=id)
	if(request.method == "GET"):
		form = LibrosForm(instance = libros)
	else:
		form = LibrosForm(request.POST, instance = libros)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'libros/update.html', context)

#DELETE

def delete(request, id):
	libros = Libros.objects.get(id = id)
	if(request.method == "POST"):
		libros.delete()
		return redirect("list")
	context = {
		"libros": libros,
	}
	return render(request, 'libros/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "libros/list2.html"
	queryset = Libros.objects.filter()
	def get_queryset(self, *args, **kwargs):
		qs = Libros.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(nombre__icontains=query) | Q(autor__icontains=query) | Q(paginas__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "libros/detail2.html"
	model = Libros

class Create(generic.CreateView):
	template_name = "libros/create2.html"
	model = Libros
	fields = [
		"nombre",
		"autor",
		"paginas",
		"cantidad",
		"electronico",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "libros/update2.html"
	model = Libros
	fields = [
		"nombre",
		"autor",
		"paginas",
		"cantidad",
		"electronico",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "libros/delete2.html"
	model = Libros
	success_url = reverse_lazy("list")
