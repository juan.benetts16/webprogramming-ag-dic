from django.core.exceptions import ValidationError

def validation_paginas(value):
	paginas = value
	if (value > 3000):
		raise ValidationError("No puedes ser tan grande")
	return value

def validation_electronico(value):
	electronico = value
	if (electronico == False):
		raise ValidationError("Tiene que estar disponible")
	return value

