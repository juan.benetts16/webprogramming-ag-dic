from .models import Computadoras
import time

def messageVersion(request):
	context = { "msgVersion" : "App de Computadoras ALPHA" ,}
	return context

def countComputadoras(request):
	context = { "countComputadoras" : Computadoras.objects.count(), }
	return context

def getDate(self):
	context = { "date" : time.strftime("%c")}
	return context