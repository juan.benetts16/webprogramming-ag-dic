from django.db import models
from django.db.models.signals import post_save , pre_save, post_delete , pre_delete
from computadoras import signals
from computadoras import validators
# Create your models here.

class Computadoras_Queryset(models.QuerySet):
	def computadoras_memoria(self):
		return self.filter(memora = 1000)
	def computadoras_ram(self):
		return self.filter(ram = 16)

class Computadoras(models.Model):
	cpu = models.CharField(max_length = 20)
	case = models.CharField(max_length = 20)
	ram = models.IntegerField(validators = [validators.validation_ram])
	memoria = models.IntegerField()
	estado = models.BooleanField()
	slug = models.CharField(max_length = 15)
	objects = Computadoras_Queryset.as_manager()

	def __str__(self):
		return self.cpu

#Signals
post_save.connect(signals.post_save_computadoras_receiver , sender = Computadoras)
pre_save.connect(signals.pre_save_computadoras_receiver , sender = Computadoras)
pre_delete.connect(signals.post_delete_computadoras_receiver , sender = Computadoras)
post_delete.connect(signals.pre_delete_computadoras_receiver  , sender = Computadoras)
