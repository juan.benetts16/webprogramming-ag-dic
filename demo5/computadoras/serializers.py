from rest_framework import serializers

from .models import Computadoras

class ComputadorasSerialize(serializers.ModelSerializer):
	class Meta:
		model = Computadoras
		fields = [
		"cpu",
		"case",
		"ram",
		"memoria",
		"estado",
		"slug",
		]