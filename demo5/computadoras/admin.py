from django.contrib import admin

# Register your models here.

from .models import Computadoras

@admin.register(Computadoras)
class ComputadorasAdmin(admin.ModelAdmin):
	list_display = [
		"cpu",
		"case",
		"ram",
		"memoria",
		"estado",
		"slug",
		]