from django import forms

from .models import Computadoras

class ComputadorasForm(forms.ModelForm):
	class Meta:
		model = Computadoras
		fields = [
		"cpu",
		"case",
		"ram",
		"memoria",
		"estado",
		"slug",
		]