from django.shortcuts import render, redirect

from django.views import generic
from django.urls import reverse_lazy
from django.db.models import Q

#Modelos, formas de alumnos
from .models import Computadoras
from .forms import ComputadorasForm

#Serializers
from .serializers import ComputadorasSerialize
from rest_framework import generics

# Create your views here.

#View Serializers
class ComputadorasAPICreate(generics.CreateAPIView):
	serializer_class = ComputadorasSerialize

	def perform_create(self, serializer):
		serializer.save()

class ComputadorasAPIList(generics.ListAPIView):
	serializer_class = ComputadorasSerialize

	def get_queryset(self, *args, **kwargs):
		return Computadoras.objects.all()

#CRUD

#CREATE
def create(request):
	form = ComputadorasForm(request.POST or None)
	if(request.user.is_authenticated):
		message = "El usuario esta loggeado"
		if form.is_valid():
			form.save()
	else:
		message = "Debe loggearse"
	context = {
		"form": form,
		"message": message,
	}
	return render(request, "computadoras/create.html", context)


#Retrieve
def list(request):
	queryset = Computadoras.objects.all()
	context = {
		'list':queryset
	}
	return render(request, 'computadoras/list.html', context)

def detail(request, id):
	queryset = Computadoras.objects.get(id=id)
	context = {
		'details':queryset
	}
	return render(request, 'computadoras/detail.html', context)

#UPDATE

def update(request, id):
	computadoras = Computadoras.objects.get(id=id)
	if(request.method == "GET"):
		form = ComputadorasForm(instance = computadoras)
	else:
		form = ComputadorasForm(request.POST, instance = computadoras)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
		"form":form,
	}
	return render(request, 'computadoras/update.html', context)

#DELETE

def delete(request, id):
	computadoras = Computadoras.objects.get(id = id)
	if(request.method == "POST"):
		computadoras.delete()
		return redirect("list")
	context = {
		"computadoras": computadoras,
	}
	return render(request, 'computadoras/delete.html', context)

#############GENERICS DEL CRUD

class List(generic.ListView):
	template_name = "computadoras/list2.html"
	queryset = Computadoras.objects.filter()
	def get_queryset(self, *args, **kwargs):
		qs = Computadoras.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(cpu__icontains=query) | Q(case__icontains=query) | Q(ram__icontains=query))
		return qs

class Detail(generic.DetailView):
	template_name = "computadoras/detail2.html"
	model = Computadoras

class Create(generic.CreateView):
	template_name = "computadoras/create2.html"
	model = Computadoras
	fields = [
		"cpu",
		"case",
		"ram",
		"memoria",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Update(generic.UpdateView):
	template_name = "computadoras/update2.html"
	model = Computadoras
	fields = [
		"cpu",
		"case",
		"ram",
		"memoria",
		"estado",
		"slug",
	]
	success_url = reverse_lazy("list")

class Delete(generic.DeleteView):
	template_name = "computadoras/delete2.html"
	model = Computadoras
	success_url = reverse_lazy("list")

